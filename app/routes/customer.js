const express = require('express');
const router = express.Router();
const CustomersController = require('../controllers/CustomersController');

router.get("/categories", CustomersController.getCategories);
router.get("/books/:key?", CustomersController.getBooks);
router.get("/books-by-category/:categoryId", CustomersController.getBooksByCategoryId);
router.get("/book/:bookId", CustomersController.getBook);

module.exports = router;
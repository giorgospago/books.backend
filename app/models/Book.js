const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require('mongoosastic');

const BookSchema = new Schema({
    title: {type: String, required: true}, // Ο τίτλος του βιβλίου
    author: String, // Το όνομα του συγγραφέα
    photo: String, // Το url της εικόνας
    description: {type: String}, // Η περιγραφή του βιβλίου
    category: {
        type: Schema.Types.ObjectId,
        ref: "Category",
        es_indexed: true,
        es_schema: require('./Category').schema
    } // Ο _id μιας κατηγορίας
});

BookSchema.plugin(mongoosastic, {
    hosts: ["5.189.177.98:39200"],
    index: "paraskevas",
    populate: [
        {path: 'category'}
    ]
});

module.exports = mongoose.model('Book', BookSchema);
const express = require('express');
const { check, validationResult } = require('express-validator/check');
const router = express.Router();
const AdminController = require('../controllers/AdminController');
const reqValidator = require('../middlewares/reqValidator');

// Πρέπει να δημιουργήσουμε έναν AdminController
router.get("/categories", AdminController.getCategories);
router.post("/categories/create", AdminController.postCategoryCreate);
router.get("/categories/:categoryId", AdminController.getCategory); // For Edit
router.put("/categories/:categoryId", AdminController.putCategoryUpdate);
router.delete("/categories/:categoryId", AdminController.deleteCategory);

router.get("/books", AdminController.getBooks);
router.post("/books/create", [
    check("photo").isURL(),
    check("title").isLength({ min: 1, max: 200}).withMessage('must be at least 1 chars long'),
    check("author").isAlpha(),
    check("description").isLength({ min: 20 }).withMessage('must be at least 20 chars long'),
    check("category").isMongoId(),
    reqValidator
], AdminController.postBookCreate);
router.get("/books/:bookId", AdminController.getBook); // For Edit
router.put("/books/:bookId", AdminController.putBookUpdate);
router.delete("/books/:bookId", AdminController.deleteBook);

module.exports = router;

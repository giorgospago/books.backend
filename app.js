const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');

const con = mongoose.connect('mongodb://5.189.177.98:37017/ParaskevasBookstore', { useNewUrlParser: true });

const app = express();
app.listen(3000);

global.Category = require('./app/models/Category').model;
global.Book = require('./app/models/Book');

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// app.get("/", (req, res) => {
app.get("/", (req, res) => {
    res.send("Welcome to Books API");
});

// to use at Customer App
app.use("/customer", require("./app/routes/customer"));

// to use at Admin App
app.use("/admin", require("./app/routes/admin"));
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mongoosastic = require('mongoosastic');

const CategorySchema = new Schema({
    name: String, // Το όνομα της κατηγορίας
});

CategorySchema.plugin(mongoosastic, {
    hosts: ["5.189.177.98:39200"],
    index: "paraskevas"
});

module.exports = {
    model: mongoose.model('Category', CategorySchema),
    schema: CategorySchema
};
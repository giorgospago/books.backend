// Όλα τα αποτελέσματα που θα στέλνουμε στο Admin App 
// Θα έρχονται από την MongoDB

const getCategories = async (req, res) => {
   try {
    const categories = await Category.find({}).exec();
    res.json(categories);
   } catch (error) {
       console.log(error);
       res.send('Error...');       
   }
};

const postCategoryCreate = async (req, res) => {
    try {
        const name = req.body.name;
    
        const c = new Category({
            name
        });
    
        await c.save();
    
        res.json({msg: "Category created !"});
    } catch (error) {
        console.log(error);
        res.send('Error...');
    }
};

const getCategory = async (req, res) => {
    try {
        const categoryId = req.params.categoryId;
        if (categoryId) {
            const category = await Category.findById(categoryId).exec();
            res.json(category);            
        }
    } catch(error) {
        console.log(error);
        res.send('Error...');
    }
};

const putCategoryUpdate = async (req, res) => {
    try {
        const categoryId = req.params.categoryId;
        if(categoryId) {
            if(req.body.name) {
                const updatedCategory = await Category.findByIdAndUpdate(categoryId, {name: req.body.name}).exec();
                return res.json({msg: "Category Updated successfully", updatedCategory});
            }
        res.json({msg: "Nothing to update..."});
        }
    } catch (error) {
        console.log(error);
        res.send('Error...');        
    }
};

const deleteCategory = async (req, res) => {
    try {
        const categoryId = req.params.categoryId;
        if(categoryId) {
            const deletedCategory = await Category.findByIdAndRemove(categoryId).exec();
            if (!deletedCategory) {
                return res.json({msg: "Category not found"});
            }
            res.json({msg: deletedCategory.name + " deleted successfully"});
        }
    } catch (error) {
        console.log(error);
        res.send('Error...');                        
    }
};

const getBooks = async (req, res) => {
    try {
        const books = await Book.find({}).populate("category").exec();
        res.json(books);
    } catch (error) {
        console.log(error);
        res.send('Error...');       
    }
};

const postBookCreate = async (req, res) => {
    try {
        const title = req.body.title;
        const author = req.body.author;
        const photo = req.body.photo;
        const description = req.body.description;
        const category = req.body.category;
        
        const b = new Book({
            title,
            author,
            photo,
            description,
            category
        });
    
        await b.save();
    
        res.send("Book created !");
    } catch (error) {
        console.log(error);
        res.send('Error...');
    }
};

const getBook = async (req, res) => {
    try {
        const bookId = req.params.bookId;
        if (bookId) {
            const book = await Book.findById(bookId).exec();
            res.json(book);            
        }
    } catch(error) {
        console.log(error);
        res.send('Error...');
    }
};

const putBookUpdate = async (req, res) => {
    try {
        const bookId = req.params.bookId;
        const fields = [
            "title",
            "author",
            "photo",
            "description",
            "category"
        ];
        const updateObj = {};
        let updateStatus = false;
        if(bookId) {
            for (field of fields) {
                if (req.body[field]) {
                    updateStatus = true;
                    updateObj[field] = req.body[field];
                }
            }
            if (updateStatus) {
                const updatedΒοοκ = await Book.findByIdAndUpdate(bookId, updateObj).exec();
                return res.json({msg: "Βοοκ Updated successfully", updatedΒοοκ});
            }
            res.json({msg: "Nothing to update..."});
        }
    } catch (error) {
        console.log(error);
        res.send('Error...');        
    }
};

const deleteBook = async (req, res) => {
    try {
        const bookId = req.params.bookId;
        if(bookId) {
            const deletedBook = await Book.findByIdAndRemove(bookId).exec();
            if (!deletedBook) {
                return res.json({msg: "Book not found"});
            }
            res.json({msg: deletedBook.title + " deleted successfully"});
        }
    } catch (error) {
        console.log(error);
        res.send('Error...');                        
    }
};
   
module.exports = {
    getCategories,
    postCategoryCreate,
    getCategory,
    putCategoryUpdate,
    deleteCategory,
    getBooks,
    postBookCreate,
    getBook,
    putBookUpdate,
    deleteBook
};
// Όλα τα αποτελέσματα που θα στέλνουμε στο Customers App 
// Θα έρχονται από τον ElasticSearch

const getCategories = (req, res) => {
    // Category.synchronize();
    Category.search(
        {
        },
        (err, results) => {
            if (err) {
                return res.json(err);
            }
            res.json(results.hits.hits.map(item => ({
                _id: item._id,
                name: item._source.name,
            })));
        }
    );
};

const getBooks = (req, res) => {
    const key = req.params.key;

    // Book.synchronize();
    // Category.synchronize();

    if (key) {
        Book.search(
            {
                query_string: {
                    query: "*" + key + "*",
                    fields: [ "title", "author", "photo", "description" ]
                }
            },
            (err, results) => {
                if (err) {
                    return res.json(err);
                }
                res.json(results.hits.hits.map(item => ({
                    _id: item._id,
                    title: item._source.title,
                    author: item._source.author,
                    photo: item._source.photo,
                    description: item._source.description,
                    category: item._source.category
                })));
            }
        );
    } else {
        Book.search(
            {
            },
            {
                size: 500
            },
            (err, results) => {
                if (err) {
                    return res.json(err);
                }
                res.json(results.hits.hits.map(item => ({
                    _id: item._id,
                    title: item._source.title,
                    author: item._source.author,
                    photo: item._source.photo,
                    description: item._source.description,
                    category: item._source.category
                })));
            }
        );
    }
};

const getBooksByCategoryId = (req, res) => {
    const categoryId = req.params.categoryId;
    // Book.synchronize();
    Book.search({
            match: {
                "category._id": categoryId
            }
        },
        (err, results) => {
            if (err) {
                return res.json(err);
            }
            res.json(results.hits.hits.map(item => ({
                _id: item._id,
                title: item._source.title,
                author: item._source.author,
                photo: item._source.photo,
                description: item._source.description,
                category: item._source.category
            })));
        }
    );
};

/* const getBook = (req, res) => {
    const bookId = req.params.bookId;
    Book.search(
        {
            query_string: {
                query: bookId,
                fields: [ "_id" ]
            }
        },
        (err, results) => {
            if (err) {
                return res.json(err);
            }
            res.json(results.hits.hits.map(item => ({
                _id: item._id,
                title: item._source.title,
                author: item._source.author,
                photo: item._source.photo,
                description: item._source.description,
                category: item._source.category
            })));
        }
    );
}; */

const getBook = (req, res) => {
    const bookId = req.params.bookId;
    Book.search(
    {
        query_string: {
            query: bookId,
            fields: [ "_id" ]
        }
    },
    (err, results) => {
        if (err) {
            return res.json(err);
        }
        const books = results.hits.hits.map(item => ({
            _id: item._id,
            title: item._source.title,
            author: item._source.author,
            photo: item._source.photo,
            description: item._source.description,
            category: item._source.category
        }));
        if (books.length > 0) {
            return res.json(books[0])
        }
        res.send("No book found");
        }
    );
};

module.exports = {
    getCategories,
    getBooks,
    getBooksByCategoryId,
    getBook
};